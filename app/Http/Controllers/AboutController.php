<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
Use App\About;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()

    {
        $about = About::all();
        return view('about.index', compact('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'judul' => 'required|unique:about',
            'content' => 'required',
              'gambar' => 'required'
        ]);

         $gambar = $request->gambar;
         $new_gambar = time().$gambar->getClientOriginalName();

        $about = About::create([
            "judul" => $request["judul"],
             "content" => $request["content"],
             "gambar" => 'uploads/about/'.$new_gambar
             
        ]);


        $gambar->move('uploads/about/', $new_gambar);

         return redirect('/about')->with('success', 'about Berhasil Disimpan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = About::find($id);

        return view('about.show', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $about = About::find($id);

        return view('about.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          
        $about = About::findorfail($id);
           if ($request->has('gambar')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('uploads/about/', $new_gambar);

        $about_data = [
            "judul" => $request["judul"],
             "content" => $request["content"],
             "gambar" => 'uploads/about/'.$new_gambar
        ];
        }
        else{
        $about_data = [
              "judul" => $request["judul"],
             "content" => $request["content"],
        ];
        }
          $about->update($about_data);
        //         $gambar = $request->gambar;
        //         $new_gambar = time().$gambar->getClientOriginalName();
         
           

        //  $update = About::where('id', $id)->update([
        //    "judul" => $request["judul"],
        //      "content" => $request["content"],
        //      "gambar" => 'uploads/about/'.$new_gambar

        // ]);

        //    $gambar->move('uploads/about/', $new_gambar);
        return redirect('/about')->with('success', 'about Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         About::destroy($id);
        return redirect('/about')->with('success', 'about Berhasil Dihapus');
    }
}
