<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
Use App\Services;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()

    {
        $services = Services::all();
        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'judul' => 'required|unique:services',
            'content' => 'required',
              'gambar' => 'required'
        ]);

         $gambar = $request->gambar;
         $new_gambar = time().$gambar->getClientOriginalName();

        $services = Services::create([
            "judul" => $request["judul"],
             "content" => $request["content"],
             "gambar" => 'uploads/services/'.$new_gambar
             
        ]);


        $gambar->move('uploads/services/', $new_gambar);

         return redirect('/services')->with('success', 'services Berhasil Disimpan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = Services::find($id);

        return view('services.show', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $services = Services::find($id);

        return view('services.edit', compact('services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          
        $services = Services::findorfail($id);
           if ($request->has('gambar')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('uploads/services/', $new_gambar);

        $services_data = [
            "judul" => $request["judul"],
             "content" => $request["content"],
             "gambar" => 'uploads/services/'.$new_gambar
        ];
        }
        else{
        $services_data = [
              "judul" => $request["judul"],
             "content" => $request["content"],
        ];
        }
          $services->update($services_data);
        //         $gambar = $request->gambar;
        //         $new_gambar = time().$gambar->getClientOriginalName();
         
           

        //  $update = Services::where('id', $id)->update([
        //    "judul" => $request["judul"],
        //      "content" => $request["content"],
        //      "gambar" => 'uploads/services/'.$new_gambar

        // ]);

        //    $gambar->move('uploads/services/', $new_gambar);
        return redirect('/services')->with('success', 'services Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Services::destroy($id);
        return redirect('/services')->with('success', 'services Berhasil Dihapus');
    }
}
