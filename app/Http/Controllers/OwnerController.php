<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
Use App\Owner;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()

    {
        $owner = Owner::all();
        return view('owner.index', compact('owner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('owner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'judul' => 'required|unique:owner',
            'content' => 'required',
              'gambar' => 'required'
        ]);

         $gambar = $request->gambar;
         $new_gambar = time().$gambar->getClientOriginalName();

        $owner = Owner::create([
            "judul" => $request["judul"],
             "content" => $request["content"],
             "gambar" => 'uploads/owner/'.$new_gambar
             
        ]);


        $gambar->move('uploads/owner/', $new_gambar);

         return redirect('/owner')->with('success', 'owner Berhasil Disimpan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = Owner::find($id);

        return view('owner.show', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $owner = Owner::find($id);

        return view('owner.edit', compact('owner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          
        $owner = Owner::findorfail($id);
           if ($request->has('gambar')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('uploads/owner/', $new_gambar);

        $owner_data = [
            "judul" => $request["judul"],
             "content" => $request["content"],
             "gambar" => 'uploads/owner/'.$new_gambar
        ];
        }
        else{
        $owner_data = [
              "judul" => $request["judul"],
             "content" => $request["content"],
        ];
        }
          $owner->update($owner_data);
        //         $gambar = $request->gambar;
        //         $new_gambar = time().$gambar->getClientOriginalName();
         
           

        //  $update = Owner::where('id', $id)->update([
        //    "judul" => $request["judul"],
        //      "content" => $request["content"],
        //      "gambar" => 'uploads/owner/'.$new_gambar

        // ]);

        //    $gambar->move('uploads/owner/', $new_gambar);
        return redirect('/owner')->with('success', 'owner Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Owner::destroy($id);
        return redirect('/owner')->with('success', 'owner Berhasil Dihapus');
    }
}
