  <aside class="main-sidebar sidebar-dark-primary elevation-4">
  <br>
  
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/adminlte/dist/img/avatar5.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info" >
          <a class="d-block">{{ Auth::user()->name }} </a>

        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
            <a href="{{ route('post.index')}}" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Post
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ route('services.index')}}" class="nav-link">
              <i class="nav-icon far fa-file"></i>
              <p>
                Services
              </p>
            </a>
          </li>
         
           <li class="nav-item">
            <a href="{{ route('category.index')}}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Category
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('about.index')}}" class="nav-link">
              <i class="nav-icon fas fa-info-circle"></i>
              <p>
                About
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ route('tag.index')}}" class="nav-link">
              <i class="nav-icon fas fa-tag"></i>
              <p>
                Tag
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('owner.index')}}" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                Owner
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('user.index')}}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User
              </p>
            </a>
          </li>
        
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>