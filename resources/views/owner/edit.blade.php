@extends('layouts.master')
@section('content')
	<div class="m-3">
	<div class="card card-primary">
	             
	              <div class="card-header">
	                <h3 class="card-title">Edit owner {{$owner->id}}</h3>
	              </div>
	              <!-- /.card-header -->
	              <!-- form start -->
	              <form role="form" action="/owner/{{$owner->id}}" method="POST"  enctype="multipart/form-data">
	              	@csrf
	              	@method('PUT')
	                <div class="card-body">
	                  <div class="form-group">
	                    <label for="judul">Nama</label>
	                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $owner->judul)}}" placeholder="Masukkan Nama" required>
	                     @error('judul')
                    	<div class="alert alert-danger">
                        {{ $message }}
                    	</div>
                		@enderror
	                  </div>

	                  <div class="form-group">
	                    <label for="content">Deskripsi</label>
	                    <input type="text" class="form-control" id="content" name="content" value="{{ old('content', $owner->content)}}" placeholder="Masukkan Deskripsi" required>
	                    @error('content')
                   		 <div class="alert alert-danger">
                        {{ $message }}  
                    	</div>
              			 @enderror
	                  </div>

	                  <div class="form-group">
                    <label for="gambar">Gambar</label>
                     <div class="form-group">
                     	<img src="	{{ asset($owner->gambar) }}" height="100px">
                     </div>
                    <div class="custom-file">
                      <input type="file" class="form-control" id="gambar" name="gambar" value="{{ old('gambar', $owner->gambar)}}"  placeholder="gambar" >
                      <label class="custom-file-label" for="gambar">Choose file</label>
                    </div>
                  </div>
	                 
	                 
	                 
	                </div>
	                <!-- /.card-body -->

	                <div class="card-footer">
	                  <button type="submit" class="btn btn-primary">Update</button>
	                </div>
	              </form>
	          
	        </div>
	    </div>

@endsection

