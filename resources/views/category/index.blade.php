@extends('layouts.master')
@section('content')

<div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">Kategori</h3>

                <div class="card-tools">
		      <a href="{{ route('category.create') }}" class="btn btn-primary">Tambah Kategori</a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              	@if(session('success'))
              	<div class="alert alert-success m-3">
              			{{ session('success') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
              		
              	</div>
              	@endif
                <table class="table">
                <thead>
			<tr>
				<th>No</th>
				<th>Nama Kategori</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($category as $result => $hasil)
			<tr>
				<td>{{ $result + $category->firstitem() }}</td>
				<td>{{ $hasil->name }}</td>
				<td>
					<form action="{{ route('category.destroy', $hasil->id )}}" method="POST">
						@csrf
						@method('delete')
					<a href="{{ route('category.edit', $hasil->id ) }}" class="btn btn-primary btn-sm">Edit</a>
					<button type="submit" class="btn btn-danger btn-sm">Delete</button>
					</form>
				</td>
			</tr>
			@empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr> 
			@endforelse

		</tbody>
               
                </table>
              </div>
              <!-- /.card-body -->
            </div>
        </div>

@endsection

