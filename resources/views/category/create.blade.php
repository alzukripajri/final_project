@extends('layouts.master')

@section('content')

  <div class="m-3">
  <div class="card card-primary">
               
                <div class="card-header">
                  <h3 class="card-title">Create Kategori</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/category" method="POST" >
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label for="name">Kategori</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', '')}}" placeholder="Masukkan kategori" required>
                       @error('name')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>
                   
                   
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                  </div>
                </form>
            
          </div>
      </div>

{{-- 
  @if(count($errors)>0)
  	@foreach($errors->all() as $error)
  	<div class="alert alert-danger" role="alert">
      {{ $error }}
	</div>  		
  	@endforeach
  @endif

  @if(Session::has('success'))
  	<div class="alert alert-success" role="alert">
      {{ Session('success') }}
	</div> 
  	
  @endif

  <form action="{{ route('category.store') }}" method="POST">
  @csrf
  <div class="form-group">
      <label>Kategori</label>
      <input type="text" class="form-control" name="name">
  </div>

  <div class="form-group">
      <button class="btn btn-primary btn-block">Simpan Kategori</button>
  </div>

  </form> --}}


@endsection