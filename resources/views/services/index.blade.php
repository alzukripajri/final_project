@extends('layouts.master')
@section('content')
	<div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">Services</h3>

                <div class="card-tools">
		      <a href="{{ route('services.create')}}" class="btn btn-primary">Tambah Services</a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              	@if(session('success'))
              	<div class="alert alert-success m-3">
              			{{ session('success') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
              		
              	</div>
              	@endif
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>ISi</th>
                      <th>Gambar</th>
                      
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse ($services as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->content}}</td>
                        <td><img src="{{ asset($value->gambar) }}" class="img-fluid" style="max-height: 100px; display: block; margin: auto; width: 100px;"></td>
                        <td style="display: flex">
                            <a href="{{ route('services.show', ['service' => $value->id] )}}" class="btn btn-info btn-sm my-1 mr-1">Show</a>
                            <a href="{{ route('services.edit', ['service' => $value->id] )}}" class="btn btn-primary btn-sm my-1 mr-1">Edit</a>
                            <form action="{{ route('services.destroy', ['service' => $value->id] )}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm my-1 mr-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="4">
                        <td>No data</td>
                    </tr>  
                @endforelse            
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
        </div>

@endsection

