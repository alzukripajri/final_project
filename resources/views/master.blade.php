<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>PT Indosistek Persada</title>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="assets/img/www.png" alt="" /></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ml-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">Services</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#team">Team</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contact</a></li>
                    
                     {{--     
                         @if (Route::has('login'))
                         
                    @auth
                           <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/home') }}">Admin</a>
                    </li>
                    @else
                             <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
                            </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Register</a>
                            </li>
                        @endif
                    @endauth
               
            @endif --}}
                    </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div class="masthead-subheading">Selamat Datang di PT Indosistek Persada</div>
                <div class="masthead-heading text-uppercase">Jadilah Pemenang <br/>di Era Digital!</div>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Telusuri Tentang Kami</a>
            </div>
        </header>
        <!-- Services-->
        <section class="page-section" id="services">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Services</h2>

                    <h3 class="section-subheading text-muted">Service dan Pelayanan yang kami berikan</h3>

                </div>
                <div class="row text-center">
                     @foreach($services as $service)
                    <div class="col-md-4">
                       
                        <span>
                            <div style="overflow: hidden; padding: 0; max-width: 350px;">
                             <img class="img-fluid" src="{{ $service->gambar}}" alt="" style="max-height: 200px; display: block; margin: auto; "/>
                            </div>
                        </span>
                        <h4 class="m-3">{{$service->judul}}</h4>
                        <p class="text-muted">{{$service->content}}</p>

                    </div>
                    @endforeach
                   
                </div>
            </div>
        </section>
        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Portfolio</h2>

                    <h3 class="section-subheading text-muted">Portfolio kami</h3>

                    

                </div>
                <div class="row">
                     @foreach($post as $posting)
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <div class="portfolio-item" style="overflow: hidden; padding: 0; max-width: 350px;"\>
                            <a >
                               
                                <img class="img-fluid" src="{{ $posting->gambar}}" alt="" style="max-height: 200px; display: block; margin: auto; width: 100%;" />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">{{ $posting->judul}}</div>
                                <div class="portfolio-caption-subheading text-muted">{{$posting->category->name}}</div>

                                 <p style="font-family: roboto">{!!$posting->content!!}</p>
                                 <p ><span class="badge badge-info" style="padding: 10px">{{ $posting->created_at->diffForHumans() }}</span></p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                
                </div>
            </div>
        </section>
        <!-- About-->
        <section class="page-section" id="about">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">About</h2>

                    <h3 class="section-subheading text-muted">Tentang kami</h3>
                </div>
                <ul class="timeline">
                   @foreach($about as $tentang)
                      <li>
                        <div class="team-member">
                       <img class="mx-auto rounded-circle" src="{{ $tentang->gambar}}" alt="" />
                     
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                {{-- <h4>2009-2011</h4> --}}
                                <h4 class="subheading">{{ $tentang->judul}}</h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted">{{ $tentang->content}}</p></div>
                        </div>
                           </div>
                    </li>
                    @endforeach
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>
                                Jadilah
                                <br />
                                Pemenang
                                <br />
                                Bersama Kami
                            </h4>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!-- Team-->
        <section class="page-section bg-light" id="team">
            <div class="container">
                <div class="text-center m-5">

                    

                    <h2 class="section-heading text-uppercase">Tim terbaik kami</h2>
                    

                </div>
                <div class="row">
                     @foreach($owner as $pemilik)
                    <div class="col-lg-4">
                        <div class="team-member">

                            <img class="mx-auto rounded-circle" src="{{ $pemilik->gambar}}" alt="" />
                            <h4>{{ $pemilik->judul}}</h4>
                            <p class="text-muted">{{$pemilik->content}}</p>

                           
                        </div>
                    </div>
                    @endforeach
                    
                </div>
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center"><p class="large text-muted">Tim terbaik kami berkerja tanpa henti untuk invoasi terbaik untuk bangsa dan negara</p></div>
                </div>
            </div>
        </section>
        <!-- Clients
        <div class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/envato.jpg" alt="" /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/designmodo.jpg" alt="" /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/themeforest.jpg" alt="" /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/creative-market.jpg" alt="" /></a>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Contact Us</h2>
                    <h3 class="section-subheading text-muted">Berinteraksi dengan kami melalui email</h3>
                </div>
                <form id="contactForm" name="sentMessage" novalidate="novalidate">
                    <div class="row align-items-stretch mb-5">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name." />
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address." />
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group mb-md-0">
                                <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number." />
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-textarea mb-md-0">
                                <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <div id="success"></div>
                        <button class="btn btn-primary btn-xl text-uppercase" id="sendMessageButton" type="submit">Send Message</button>
                    </div>
                </form>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-left">Copyright © Indosistek 2021</div>
                    <div class="col-lg-4 my-3 my-lg-0">
                        <a class="btn btn-dark btn-social mx-2" href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <div class="col-lg-4 text-lg-right">
                        <a class="mr-3" href="#">Privacy Policy</a>
                        <a href="#!">Terms of Use</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Portfolio Modals-->
       
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>

    </body>
</html>
