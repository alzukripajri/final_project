@extends('layouts.master')
@section('content')

    <div class="m-3">
  <div class="card card-primary">
               
                <div class="card-header">
                  <h3 class="card-title">Create User</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/user" method="POST" >
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label for="name">Nama User</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', '')}}" placeholder="Masukkan Nama User" required>
                       @error('name')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>

                     <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email" value="{{ old('email', '')}}" placeholder="Masukkan Email" required>
                       @error('email')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>

                     <div class="form-group">
                      <label for="tipe">Tipe User</label>
                      
                      <select class="form-control" name="tipe">
                        <option value="" holder>Pilih Tipe User</option>
                        <option value="1" holder>Administrator</option>
                        <option value="0" holder>Penulis</option>
                      </select>
                       @error('tipe')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>

                     <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" id="password" name="password" value="{{ old('password', '')}}" placeholder="Masukkan Password" required>
                       @error('password')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>
                   
                   
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                  </div>
                </form>
            
          </div>
      </div>
{{-- 
  @if(count($errors)>0)
  	@foreach($errors->all() as $error)
  	<div class="alert alert-danger" role="alert">
      {{ $error }}
	</div>  		
  	@endforeach
  @endif

  @if(Session::has('success'))
  	<div class="alert alert-success" role="alert">
      {{ Session('success') }}
	</div> 
  	
  @endif

  <form action="{{ route('user.store') }}" method="POST">
  @csrf
  <div class="form-group">
      <label>Nama User</label>
      <input type="text" class="form-control" name="name">
  </div>

  <div class="form-group">
      <label>Email</label>
      <input type="email" class="form-control" name="email">
  </div>

  <div class="form-group">
      <label>Tipe User</label>
      <select class="form-control" name="tipe">
      	<option value="" holder>Pilih Tipe User</option>
      	<option value="1" holder>Administrator</option>
      	<option value="0" holder>Penulis</option>
      </select>
  </div>

   <div class="form-group">
      <label>Password</label>
      <input type="text" class="form-control" name="password">
  </div>

  <div class="form-group">
      <button class="btn btn-primary btn-block">Simpan User</button>
  </div>

  </form> --}}


@endsection