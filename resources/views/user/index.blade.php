@extends('layouts.master')
@section('content')

 <div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">Users</h3>

                <div class="card-tools">
		      <a href="{{ route('user.create') }}" class="btn btn-primary">Tambah User</a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              	@if(session('success'))
              	<div class="alert alert-success m-3">
              			{{ session('success') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
              		
              	</div>
              	@endif
                <table class="table">
                <thead>
			<tr>
				<th>No</th>
				<th>Nama User</th>
				<th>Email</th>
				<th>Type</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($user as $result => $hasil)
			<tr>
				<td>{{ $result + $user->firstitem() }}</td>
				<td>{{ $hasil->name }}</td>
				<td>{{ $hasil->email }}</td>
				<td>
					@if($hasil->tipe)
						<span class="badge badge-info">Administrator</span>
						@else
						<span class="badge badge-warning">Penulis</span>
					@endif

				</td>
				<td>
					<form action="{{ route('user.destroy', $hasil->id )}}" method="POST">
						@csrf
						@method('delete')
					<a href="{{ route('user.edit', $hasil->id ) }}" class="btn btn-primary btn-sm">Edit</a>
					<button type="submit" class="btn btn-danger btn-sm">Delete</button>
					</form>
				</td>
			</tr>
			@empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr> 
			@endforelse

		</tbody>

	</table>
	{{ $user->links() }}
	 </div>
              <!-- /.card-body -->
            </div>
        </div>

@endsection