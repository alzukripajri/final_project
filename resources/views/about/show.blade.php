@extends('layouts.master')
@section('content')
<div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">{{ $show->judul }}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body text-center">
               
                <img src="{{asset($show->gambar)}}"  style="width: 100px; ">
              	
                <p style="margin-top: 20px">{{ $show->content }}</p>
               
              <!-- /.card-body -->

            </div>
        </div>
    </div>

@endsection