@extends('layouts.master')
@section('content')
	<div class="m-3">
	<div class="card card-primary">
	             
	              <div class="card-header">
	                <h3 class="card-title">Create Tentang Kami</h3>
	              </div>
	              <!-- /.card-header -->
	              <!-- form start -->
	              <form role="form" action="/about" method="POST" enctype="multipart/form-data">
	              	@csrf
	                <div class="card-body">
	                  <div class="form-group">
	                    <label for="judul">Judul</label>
	                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '')}}" placeholder="Masukkan Judul" required>
	                     @error('judul')
                    	<div class="alert alert-danger">
                        {{ $message }}
                    	</div>
                		@enderror
	                  </div>

	                  <div class="form-group">
	                    <label for="content">Isi</label>
	                    <input type="text" class="form-control" id="content" name="content" value="{{ old('content', '')}}" placeholder="content" required>
	                    @error('content')
                   		 <div class="alert alert-danger">
                        {{ $message }}
                    	</div>
              			 @enderror
	                  </div>

	                  <div class="form-group">
                    <label for="gambar">Gambar</label>

                    <div class="custom-file">
                      <input type="file" class="form-control" id="gambar" name="gambar" placeholder="gambar" required>
                      <label class="custom-file-label" for="gambar">Choose file</label>
                    </div>
                  </div>
	                 
	                 
	                 
	                </div>
	                <!-- /.card-body -->

	                <div class="card-footer">
	                  <button type="submit" class="btn btn-primary">Create</button>
	                </div>
	              </form>
	          
	        </div>
	    </div>

@endsection

