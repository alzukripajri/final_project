@extends('layouts.master')
@section('content')

	<div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">Posts</h3>

                <div class="card-tools">
		      <a href="{{ route('post.create') }}" class="btn btn-primary">Tambah Posts</a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              	@if(session('success'))
              	<div class="alert alert-success m-3">
              			{{ session('success') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
              		
              	</div>
              	@endif
                <table class="table">
                <thead>
			<tr>
				<th>No</th>
				<th>Nama Post</th>
				<th>Kategori</th>
				<th>Daftar Tags</th>
				<th>Creator</th>
				<th>Thumbnail</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($post as $result => $hasil)
			<tr>
				<td>{{ $result + $post->firstitem() }}</td>
				<td>{{ $hasil->judul }}</td>
				<td>{{ $hasil->category->name }}</td>
				<td>@foreach($hasil->tags as $tag)
					<ul>
						<h6><span class="badge badge-info">{{ $tag->name }}</span></h6>
					</ul>
					@endforeach		
				</td>
				<td>{{$hasil->users->name }}</td>
				<td><img src="{{ asset( $hasil->gambar ) }}" class="img-fluid" style="max-height: 100px; display: block; margin: auto; width: 100px;"></td>
				 <td style="display: flex">
                            <a href="{{ route('post.show', ['post' => $hasil->id] )}}" class="btn btn-info btn-sm my-1 mr-1">Show</a>
                            <a href="{{ route('post.edit', ['post' => $hasil->id] )}}" class="btn btn-primary btn-sm my-1 mr-1">Edit</a>
                            <form action="{{ route('post.destroy', ['post' => $hasil->id] )}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm my-1 mr-1" value="Delete">
                            </form>
                        </td>
				{{-- <td>
					<form action="{{ route('post.destroy', $hasil->id )}}" method="POST">
						@csrf
						@method('DELETE')
					<a href="{{ route('post.edit', $hasil->id ) }}" class="btn btn-primary btn-sm">Edit</a>
					 <a href="{{ route('post.show', $hasil->id )}}" class="btn btn-info btn-sm my-1 mr-1">Show</a>
					<button type="submit" class="btn btn-danger btn-sm">Delete</button>
					</form>
				</td> --}}
			</tr>
			@empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr> 
			@endforelse

		</tbody>


	</table>
	{{ $post->links() }}

	 </div>
              <!-- /.card-body -->
            </div>
        </div>


@endsection