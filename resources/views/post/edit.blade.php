@extends('layouts.master')
@section('sub-judul','Edit Post')
@section('content')



 <div class="m-3">
  <div class="card card-primary">
               
                <div class="card-header">
                  <h3 class="card-title">Update Post</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="card-body">
                    

                    <div class="form-group">
                      <label for="judul">Judul</label>
                      <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $post->judul)}}" placeholder="Masukkan Judul" required>
                       @error('judul')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>

                    <div class="form-group">
                      <label>Kategori</label>
                      <select class="form-control" name="category_id">
                        <option value="" holder>Pilih Kategori</option>
                          @foreach($category as $result)
                          <option value="{{ $result->id }}"
                          @if($result->id == $post->category_id)
                            selected
                          @endif
                            >{{  $result->name }}</option>
                          @endforeach
                      </select>
                       @error('category_id')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>
                   


                <div class="form-group">
                    <label>Pilih Tags</label>
                    <div class="select2-primary">
                    <select class="form-control select2" data-dropdown-css-class="select2-primary" multiple="multiple" name="tags[]">
                         @foreach($tags as $tag)
                          <option value="{{ $tag->id }}"
                          @foreach($post->tags as $value)
                            @if($tag->id == $value->id)
                            selected
                            @endif
                          @endforeach         
                            >{{ $tag->name }}</option> 
                          @endforeach
                    </select>
                  </div>
                </div>


                <div class="form-group">
                    <label>Konten</label>
                    <textarea class="form-control" name="content" id="content">{{ $post->content }}</textarea>
                </div>
                <div class="form-group">
                    <label>Thumbnail</label>
                     <div class="form-group">
                      <img src="  {{ asset($post->gambar) }}" height="100px">
                     </div>
                     <div class="custom-file">
                      <input type="file" class="form-control" id="gambar" name="gambar" placeholder="gambar" >
                      <label class="custom-file-label" for="gambar">Choose file</label>
                    </div>
                </div>                      
                   
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
            
          </div>
      </div>


<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script >
  CKEDITOR.replace( 'content' );

</script>

{{--Bbatas--}}


@endsection