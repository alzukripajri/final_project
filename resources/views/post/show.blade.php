@extends('layouts.master')
@section('content')
<div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">{{ $show->judul }}</h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body text-center">
               
                <img src="{{asset($show->gambar)}}"  style="width: 100px; ">
                 <h4 style="margin-top: 20px">{{$show->category->name}}</h4>
              	
                <p style="margin-top: 20px">{!! $show->content !!}</p>
               
              <!-- /.card-body -->

            </div>
            <button type="button" class="btn btn-danger btn-sm my-1 mr-1" >Warning</button>
        </div>
    </div>

@endsection