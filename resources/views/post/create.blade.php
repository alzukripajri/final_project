@extends('layouts.master')
@section('content')


  <div class="m-3">
  <div class="card card-primary">
               
                <div class="card-header">
                  <h3 class="card-title">Create Post</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/post" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="card-body">
                    

                    <div class="form-group">
                      <label for="judul">Judul</label>
                      <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '')}}" placeholder="Masukkan Judul" required>
                       @error('judul')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>

                    <div class="form-group">
                      <label>Kategori</label>
                      <select class="form-control" name="category_id">
                        <option value="" holder>Pilih Kategori</option>
                        @foreach($category as $result)
                        <option value="{{ $result->id }}">{{  $result->name }}</option>
                        @endforeach
                      </select>
                       @error('category_id')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>
                   


                <div class="form-group">
                    <label>Pilih Tags</label>
                    <div class="select2-primary">
                    <select class="form-control select2" data-dropdown-css-class="select2-primary" multiple="multiple" name="tags[]">
                        @foreach($tags as $tag)
                        <option value="{{ $tag->id }}">{{ $tag->name }}</option> 
                        @endforeach
                    </select>
                  </div>
                </div>


                <div class="form-group">
                    <label>Konten</label>
                    <textarea class="form-control" name="content" id="content"></textarea>
                </div>
                <div class="form-group">
                    <label>Thumbnail</label>
                     <div class="custom-file">
                      <input type="file" class="form-control" id="gambar" name="gambar" placeholder="gambar" required>
                      <label class="custom-file-label" for="gambar">Choose file</label>
                    </div>
                </div>                      
                   
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                  </div>
                </form>
            
          </div>
      </div>


<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script >
  CKEDITOR.replace( 'content' );

</script>

@endsection