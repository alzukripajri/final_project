@extends('layouts.master')
@section('content')

 <div class="m-3">
  <div class="card card-primary">
               
                <div class="card-header">
                  <h3 class="card-title">Edit Tag {{$tags->id}}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/tag/{{$tags->id}}" method="POST"  enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="card-body">
                    <div class="form-group">
                      <label for="name">Tag</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $tags->name)}}" placeholder="Masukkan kategori" required>
                       @error('name')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>

                   
                   
                  </div>
                  <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
            
          </div>
      </div>



@endsection