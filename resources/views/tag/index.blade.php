@extends('layouts.master')
@section('content')

 <div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">Tags</h3>

                <div class="card-tools">
		      <a href="{{ route('tag.create') }}" class="btn btn-primary">Tambah Tags</a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              	@if(session('success'))
              	<div class="alert alert-success m-3">
              			{{ session('success') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
              		
              	</div>
              	@endif
                <table class="table">
                <thead>
			<tr>
				<th>No</th>
				<th>Nama Tag</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($tag as $result => $hasil)
			<tr>
				<td>{{ $result + $tag->firstitem() }}</td>
				<td>{{ $hasil->name }}</td>
				<td>
					<form action="{{ route('tag.destroy', $hasil->id )}}" method="POST">
						@csrf
						@method('delete')
					<a href="{{ route('tag.edit', $hasil->id ) }}" class="btn btn-primary btn-sm">Edit</a>
					<button type="submit" class="btn btn-danger btn-sm">Delete</button>
					</form>
				</td>
			</tr>
			@empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr> 
			@endforelse

		</tbody>

	  </table>
	  {{ $tag->links() }}
              </div>
              <!-- /.card-body -->
            </div>
        </div>


@endsection