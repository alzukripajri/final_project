@extends('layouts.master')
@section('content')

    <div class="m-3">
  <div class="card card-primary">
               
                <div class="card-header">
                  <h3 class="card-title">Create Tag</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/tag" method="POST" >
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label for="name">Tag</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', '')}}" placeholder="Masukkan kategori" required>
                       @error('name')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>
                   
                   
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                  </div>
                </form>
            
          </div>
      </div>


@endsection