<h1>Final Project Laravel Batch 20 by Kelompok 1</h1>
<h1>Website Company Profile dengan Laravel 6</h1>

<h2> Anggota Kelompok</h2>
<p>
1. Irfan Kusfany<br/>
2. Pajri Al Zukri<br/>
3. Vincent<br/>
</p>

<h2>Website Demo</h2>
<p>Front End: <a href="http://finalproject.indosistek.co.id" target="_blank">http://finalproject.indosistek.co.id</a></p>
<p>Back End: <a href="http://finalproject.indosistek.co.id/public/login" target="_blank">http://finalproject.indosistek.co.id/public/login</a></p>

<h2>ERD Project</h2>
<p>File PNG: <a href="https://gitlab.com/alzukripajri/final_project/-/blob/master/public/erd-final-project.png" target="_blank">ERD Final Project PNG</a></p>
<p>File MWB: <a href="https://gitlab.com/alzukripajri/final_project/-/blob/master/public/erd-final-project.mwb" target="_blank">ERD Final Project MWB</a></p>

<h2>Video Penjelasan Project</h2>
<p>Video Penjelasan on Youtube: <a href="https://www.youtube.com/watch?v=ws5NZPgKRIo" target="_blank">Video Penjelasan</a></p>